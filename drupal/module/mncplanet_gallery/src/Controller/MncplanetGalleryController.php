<?php

namespace Drupal\mncplanet_gallery\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Tags;
use Drupal\Component\Utility\Unicode;


Class MncplanetGalleryController {

  public function gallery()
  {
     return array(
       '#title' => 'Mncplanet Gallery',
        '#markup' => 'This is some content'
     );

  }


   /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request, $field_name, $count) {
    $results = [];

    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {
      $typed_string = Tags::explode($input);
      $typed_string = Unicode::strtolower(array_pop($typed_string));
      // @todo: Apply logic for generating results based on typed_string and other
      // arguments passed.

      $connection_mnc_auto = \Drupal::service('database');
      $query_mnc_auto = $connection_mnc_auto->query("SELECT tid,name FROM {taxonomy_term_field_data}  where status = 1 and name like '%".$typed_string."%' ");
      $result_mnc_auto = $query_mnc_auto->fetchAll();
      foreach ($result_mnc_auto as $record) {
        $i = $record->tid;
        $name = $record->name;
        $results[] = [
          'value' => $name . '(' . $i . ')',
          'label' => $name,
        ];

      }



    }

    return new JsonResponse($results);
  }

}
