<?php

namespace Drupal\mncplanet_gallery\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Mncplanet' Block.
 *
 * @Block(
 *   id = "mnc-gallery_block",
 *   admin_label = @Translation("Mnc Gallery block"),
 *   category = @Translation("Mncplanet Gallery"),
 * )
 *
 *

 * SELECT entity_id FROM test_d8.mncnode__field_tags
*  where field_tags_target_id in
*  (SELECT tid FROM test_d8.mnctaxonomy_term_field_data where name = 'Home gallery');


 *
 *
 *
 *
 */
class MncGalleryBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {



    preg_match('#\((.*?)\)#',$this->configuration['mnc_gallery_block_name'], $match); // get value in round brackets
    $mnc_gallery_block_taxonomy_term_id = $match[1];
    $connection_mnc = \Drupal::service('database');
    $query_mnc = $connection_mnc->query("SELECT entity_id FROM {node__field_tags}
    where field_tags_target_id in
    (".$mnc_gallery_block_taxonomy_term_id.")");
     $result_mnc = $query_mnc->fetchAll();


     $controls = '<a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
     <span class="sr-only">Previous</span>
     </a>
     <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
     <span class="carousel-control-next-icon" aria-hidden="true"></span>
     <span class="sr-only">Next</span>
     </a>';

     $indicators = '<ol class="carousel-indicators">';
     $mnc_counter  = 0 ;
     $slides_items = '';
     foreach ($result_mnc as $record) {

      $nid = $record->entity_id;
      $node = \Drupal\node\Entity\Node::load($nid);
      $node_title =  $node->getTitle();
      $node_img = file_create_url($node->field_image->entity->getFileUri());
      $node_link = $node->toUrl()->toString();
      $node_summary = $node->get('body')->summary;

      if($mnc_counter==0) {
        $indicators .= '<li data-target="#carousel-example-2" data-slide-to="'.$mnc_counter.'" class="active"></li>';
        $slides_items .= '<div class="carousel-item active">';
      } else {
        $indicators .='<li data-target="#carousel-example-2" data-slide-to="'.$mnc_counter.'"></li>';
        $slides_items .= '<div class="carousel-item ">';
      }

      $slides_items .= '<div class="view">
      <img class="d-block w-100" src="'.$node_img.'" alt="'.$node_title.'">
      <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption mnc-caption" >
      <h3 class="h3-responsive"><a href= "'.$node_link.'">'.$node_title.'</a></h3>
      <p>'.$node_summary.'</p>
      </div>';

      $slides_items .= '</div>';
      $mnc_counter++;
     }

     $indicators .= '</ol>';


    $slide_items_container = '<div class="carousel-inner" role="listbox">';
    $slide_items_container .=  $slides_items;
    $slide_items_container .= '</div>';

    $slide ='<div id="carousel-example-2" class="carousel slide carousel-fade z-depth-1-half" data-ride="carousel">';
    $slide .= $indicators;
    $slide .= $slide_items_container;
    $slide .= $controls;
    $slide .= '</div>';


    return array(
      '#markup' => $this->t($slide),
    );
  }


  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['mnc_gallery_block_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Taxonomy term name'),
      '#description' => $this->t('Taxonomy term name that you want to show in gallery?'),
      '#default_value' => isset($config['mnc_gallery_block_name']) ? $config['mnc_gallery_block_name'] : '',
      '#autocomplete_route_name' => 'mncplanet_gallery.autocomplete',
      '#autocomplete_route_parameters' => array('field_name' => 'name', 'count' => 5),
    ];

    return $form;
  }

  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['mnc_gallery_block_name'] = $values['mnc_gallery_block_name'];
  }

}
