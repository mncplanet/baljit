<?php
Class ClassException
{
  public $result;
  public function  __construct()
  {
	  $this->result= 0;
  }
  
  public function divideNumberE($a,$b)
  {
	if(!$b)
	{		
	throw new Exception('Division by zero.');
    }	
    $this->result =  $a/$b;
	$this->result = $this->result+100;	
  }
  
  public function divideNumber($a,$b)
  {
		 
		 if(!$b){
			 echo '<br>Division by zero.';
			
		 } else {
			  $this->result =  $a/$b;
			  $this->result = $this->result+100;
		 }
		
 }
	
	
	
	
}

try {
// Exception: test case 1
$obj1 = new ClassException();
$obj1->divideNumberE(10,2);
echo '<br>Result -> ',$obj1->result;
echo '<br>','Test';
// Exception: test case 2
$obj2 = new ClassException();
$obj2->divideNumberE(10,0);
echo '<br>Result -> ',$obj2->result;
echo '<br>','Test';
// Exception: test case 3
$obj3 = new ClassException();
$obj3->divideNumberE(10,5);
echo '<br>Result -> ',$obj3->result;
echo '<br>','Test';

}
catch (Exception $e) {
	echo "<br> Error : ",$e->getMessage();
	echo "<br>Line no.: ",$e->getLine();
	echo "<br>File: ",$e->getFile();
	 
}
finally {
	echo '<br>This block always excecuted ';
}

echo '<hr>';
// No Exception model : test case 1
$obj4 = new ClassException();
$obj4->divideNumber(10,2);
echo '<br>Result -> ',$obj4->result;
echo '<br>','Test';
// No Exception model : test case 2
$obj5 = new ClassException();
$obj5->divideNumber(10,0);
echo '<br>Result -> ',$obj5->result;
echo '<br>','Test';
// No Exception model : test case 3
$obj6 = new ClassException();
$obj6->divideNumber(10,5);
echo '<br>Result -> ',$obj6->result;
echo '<br>','Test';


?>