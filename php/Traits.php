<?php

trait Contact {
	
	function getContact()
	{
	  echo '<br> Contact Detail';
	  echo '<br> Phone : 604-1111-0000';
      echo '<br> Email : test123@gmail.com';
      echo '<br> East Hastings Street, Vancouver, BC';	  
		
	}
	
	
}

class College 
{
  const NAME = 'ABC Engineering College';	
	
}

class Employee extends College {
	use Contact;
	
}

class Student extends College {
	use Contact;
	
}

$objEmp = new Employee();
$objStud = new Student();
$objEmp->getContact();
echo '<hr>';
$objStud-> getContact();


?>