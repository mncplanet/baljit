<?php

trait Student {
	
	function getContact()
	{
	  echo '<br> Contact Detail For Student';
	  echo '<br> Phone : 604-1111-0000';
      echo '<br> Email : student123@gmail.com';
      echo '<br> East Hastings Street, Vancouver, BC';	  
		
	}
	
	
}

trait Employee {
	
	function getContact()
	{
	  echo '<br> Contact Detail For Employee';
	  echo '<br> Phone : 604-1111-9999';
      echo '<br> Email : employee123@gmail.com';
      echo '<br> burrard Street, Vancouver, BC';	  
		
	}
	
	
}

class College 
{
  use Student, Employee {
        Student::getContact as getContactForStudent;
        Employee::getContact insteadof Student;
    }
	
}

$objCollege = new College();
$objCollege->getContactForStudent();
echo '<hr>';
$objCollege->getContact();





?>