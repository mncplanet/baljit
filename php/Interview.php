<?php
// Q1: null and empty
$v1 = null;
$v2 = "";
$v3 = ' ';
$v4 = "''";
$r1 = empty($v1);
$r2 = empty($v2);
$r3 = empty($v3);
$r4 = empty($v4);
echo '<br> Response 1 :'.$r1;
echo '<br> Response 2 :'.$r2;
echo '<br> Response 3 :'.$r3;
echo '<br> Response 4 :'.$r4;
// Q2 print PHP code
echo '<pre>';
echo '$v1 = null;';
echo '</pre>';



?>