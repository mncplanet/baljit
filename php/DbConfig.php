<?php
// Filename : DbConfig.php
class DbConfig {

   const SERVERNAME = 'localhost';
   const USERNAME = 'root';
   const PASSWORD  = 'root'; 
   const DBNAME = 'mydb';
   public $defaultConn;

   function __construct($servername = self::SERVERNAME,$username = self::USERNAME,$password= self::PASSWORD,$dbname = self::DBNAME) {
    $this->defaultConn = new mysqli($servername,$username,$password,$dbname);
     if ($this->defaultConn->connect_error) {
        die("Connection failed: " . $this->defaultConn->connect_error);
    } 
   
   }
   public function getConnection() {
       return $this->defaultConn;
   }

  }



?>