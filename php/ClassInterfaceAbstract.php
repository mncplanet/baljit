<?php

abstract class Employee
{
protected $id ;
protected $name;
protected $hourlyRate;
protected $grandSalary;
public function __construct($id ,$name,$hourlyRate)
{
	$this->id = $id;
	$this->name =$name;
	$this->hourlyRate =$hourlyRate;
}

public function basicSalary($weeklyHours)
{
  $this->grandSalary =  $this->hourlyRate * $weeklyHours;
}
	
}

interface EmployeeInterface
{
	public function getRecord();
	public function  getHoursWorked($weeklyHours);
	public function  getDeduction($deduction);
	public function  salaryCalculation();
	
}



Class EmployeeDetails extends Employee implements EmployeeInterface
{
private $weeklyHours;
private $deductionPct;
private $deductionAmt;
private $netSalary;
public function __construct($id ,$name,$hourlyRate)
{
	parent::__construct($id ,$name,$hourlyRate);
}


public function getHoursWorked($weeklyHours)
{
$this->weeklyHours = $weeklyHours;
}
public function getDeduction($deductionPct)
{
 $this->deductionPct = $deductionPct;
}

public function salaryCalculation ()
{
 parent::basicSalary($this->weeklyHours);
 $this->deductionAmt = ($this->deductionPct * $this->grandSalary ) /100;
 $this->netSalary = $this->grandSalary - $this->deductionAmt;
}	
public function getRecord()
{
echo '&lt;tr&gt;';
echo '&lt;td&gt;',$this->id,'&lt;/td&gt;';
echo '&lt;td&gt;',$this->name,'&lt;/td&gt;';
echo '&lt;td&gt;$',$this->hourlyRate,'&lt;/td&gt;';
echo '&lt;td&gt;$',$this->grandSalary,'&lt;/td&gt;';
echo '&lt;td&gt;$',$this->deductionAmt,'&lt;/td&gt;';
echo '&lt;td&gt;$',$this->netSalary,'&lt;/td&gt;';
echo '&lt;/tr&gt;';
}	
	
}

$objEmplyee1 = new EmployeeDetails(100,'Baljit Singh',15);
$objEmplyee1->getHoursWorked(40);
$objEmplyee1->getDeduction(5);
$objEmplyee1->salaryCalculation();
echo '&lt;table border =1&gt;';
echo '&lt;tr&gt;&lt;td&gt;Id&lt;/td&gt;&lt;td&gt;Name&lt;/td&gt;&lt;td&gt;Hourly Rate	&lt;/td&gt;&lt;td&gt;Grand Salary&lt;/td&gt;&lt;td&gt;Deduction&lt;/td&gt;&lt;td&gt;Net Salary&lt;/td&gt;&lt;/tr&gt;';
$objEmplyee1->getRecord();
echo '&lt;/table>'


?>