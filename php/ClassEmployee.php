<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class ClassEmployee
{
    const  COMPANY = 'MncPlanet';
    static $recordCounter = 100;
    private $id;
    private $name ;
    private $phone;
    protected $dept;
    protected $salary;
    
   public static function getCOMPANY() {
      return self::COMPANY;
    }
    
    function __construct($name ='', $phone='', $dept='', $salary='') {
     $this->id =  static::$recordCounter++;
     $this->name = $name;
     $this->phone = $phone;
     $this->dept = $dept;
     $this->salary = $salary;
     
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
         $this->name = $name;
    }
    public function getPhone()
    {
        return $this->phone;
    }
    public  function setPhone($phone)
    {
         $this->phone = $phone;
    }

    public function getDept()
    {
        return $this->dept;
    }
    public function setDept($dept)
    {
         $this->dept = $dept;
    }

    public function getSalary()
    {
        return $this->salary;
    }
    public function setSalary($salry)
    {
         $this->salary = $salary;
    }
    public function getRecord()
    {
        
        $record ='<td>'.$this->getId().'</td>';
        $record .='<td>'.$this->getName().'</td>';
        $record .='<td>'.$this->getPhone().'</td>';
        $record .='<td>'.$this->getDept().'</td>';
        $record .='<td>'.$this->getSalary().'</td>';
       
        return $record;

        
    }

}


class EmployeeDetails extends ClassEmployee
{
 
private $address;
 function __construct($name ='', $phone='', $dept='', $salary='',$address ='')
 {
    parent::__construct($name,$phone,$dept);
    $this->salary = $salary;
    $this->address = $address;
    
 }

 public function getAddress()
 {
     return $this->address;
 }

 public function getRecord() {
   $record = parent:: getRecord();
   $record .= '<td>'.$this->getAddress().'</td>';
   return $record;
 }

}

 
  $employee1 = new EmployeeDetails('Baljit Singh','604-771-0000','Web development','4000','Khan Piara');
  $employee2 = new EmployeeDetails('Satpal Kaur','778-892-0000','Hr dept','3000','Adampur');
  $employee3 = new EmployeeDetails();
  $employee3->setName('Harmeet Singh');

  echo '<table border=1 width = "500px">';
  echo '<tr><td colspan =6>', 'Companey name:  <b>',ClassEmployee::getCOMPANY(),'</b></td></tr>';
  echo  '<tr>',$employee1->getRecord(), '</tr>';
  echo  '<tr>',$employee2->getRecord(),'</tr>';
  echo  '</tr>',$employee3->getRecord(),'</tr>';

  echo '</table>';

   

?>