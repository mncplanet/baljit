<?php
namespace gallery\gallery1;

class MyGallery 
{
 const NAME = 'This Gallery1 developed by Baljit Singh using PHP 7.';	

 function getName()
 {
  return self::NAME;
 } 
}

namespace gallery\gallery2;

class MyGallery 
{
 const NAME = 'This Gallery2 developed by Baljit Singh using PHP 7.';	

 function getName()
 {
  return self::NAME;
 } 
	
}

use gallery\gallery1 as g1;
use gallery\gallery2 as g2;


$obj1 =  new g1\MyGallery();
echo "<br>",$obj1->getName();

$obj2 =  new g2\MyGallery();
echo "<br>",$obj2->getName();

?>